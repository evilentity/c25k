package io.piotrjastrzebski.c25k.data;

public enum WorkoutType {
    RUN, WALK, WARMUP, COOLDOWN
}
