package io.piotrjastrzebski.c25k.data;

import io.piotrjastrzebski.c25k.utils.TextUtils;
import lombok.Data;

@Data
public class WorkoutStep {
    public WorkoutType type;
    public int from; // seconds from start
    public int to; // seconds from start

    public WorkoutStep from (int minutes, int seconds) {
        from = minutes * 60 + seconds;
        return this;
    }

    public WorkoutStep to (int minutes, int seconds) {
        to = minutes * 60 + seconds;
        return this;
    }

    public int duration () {
        return to - from;
    }
}
