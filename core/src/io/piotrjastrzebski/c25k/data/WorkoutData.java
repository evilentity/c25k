package io.piotrjastrzebski.c25k.data;

import com.badlogic.gdx.utils.Array;
import lombok.Data;

/**
 * Const data about specific workout day
 */
@Data
public class WorkoutData {
    public int id;
    public int week;
    public Array<WorkoutStep> steps = new Array<>();

    /**
     * @return duration of the workout
     */
    public int duration () {
        int duration = 0;
        for (WorkoutStep step : steps) {
            // they overlap, do we -1?
            duration += step.duration();
        }
        return duration;
    }
}
