package io.piotrjastrzebski.c25k.data;

import lombok.Data;

/**
 * Used for storing user progress
 */
@Data
public class Workout {
    public int id;
    public boolean completed;
    public int stepId = 0;
    public int stepProgressSeconds = 0;
    public transient WorkoutData data;

    public Workout () {

    }

    public Workout (WorkoutData data) {
        this.data = data;
        this.id = data.id;
    }

    public int progress () {
        if (completed) return 100;
        int totalProgressSeconds = 0;
        for (int i = 0; i < stepId; i++) {
            totalProgressSeconds += data.steps.get(i).duration();
        }
        totalProgressSeconds += stepProgressSeconds;
        return (int)(totalProgressSeconds/(float)data.duration() * 100);
    }

    @Override public String toString () {
        return "Workout{" + "id=" + id + ", completed=" + completed + '}';
    }
}
