package io.piotrjastrzebski.c25k;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import io.piotrjastrzebski.c25k.data.*;
import io.piotrjastrzebski.c25k.screens.LoadingScreen;
import io.piotrjastrzebski.c25k.screens.WorkoutsScreen;
import io.piotrjastrzebski.c25k.utils.Assets;
import io.piotrjastrzebski.c25k.utils.Log;
import io.piotrjastrzebski.c25k.utils.Sounds;
import io.piotrjastrzebski.c25k.utils.TextUtils;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class C25KApp extends Game {
    SpriteBatch batch;
    ShapeRenderer shapes;
    Preferences prefs;
    boolean debug = false;
    private final static String PREF_WORKOUTS = "workouts";
    private final static String PREF_SOUND = "sound";
    private final static String PREF_DEBUG = "debug";

    ScheduledExecutorService ses;

    public Assets assets;

    Json json;
    Array<WorkoutData> workoutData;
    Workouts workouts;

    @Override
    public void create () {
        batch = new SpriteBatch();
        batch.enableBlending();
        shapes = new ShapeRenderer();
        prefs = Gdx.app.getPreferences("prefs.xml");
        assets = new Assets();

        {
            json = new Json();
            json.setIgnoreUnknownFields(true);
            json.setSerializer(WorkoutStep.class, new Json.Serializer<WorkoutStep>() {
                @Override public void write (Json json, WorkoutStep object, Class knownType) {
                    json.writeObjectStart();
                    json.writeValue("type", object.type);
                    json.writeValue("from", TextUtils.formatDuration(object.from));
                    json.writeValue("to", TextUtils.formatDuration(object.to));
                    json.writeObjectEnd();
                }

                @Override public WorkoutStep read (Json json, JsonValue jsonData, Class type) {
                    WorkoutStep step = new WorkoutStep();
                    step.type = json.readValue(WorkoutType.class, jsonData.get("type"));
                    step.from = TextUtils.parseDuration(jsonData.getString("from"));
                    step.to = TextUtils.parseDuration(jsonData.getString("to"));
                    return step;
                }
            });
            json.setUsePrototypes(false);
            workoutData = json.fromJson(Array.class, WorkoutData.class, Gdx.files.internal("workouts.gjson"));
            Log.info(workoutData.toString());

            String persisted = prefs.getString(PREF_WORKOUTS, null);
            if (persisted != null) {
                workouts = json.fromJson(Workouts.class, persisted);
                for (Workout workout : workouts) {
                    workout.data = workoutData.get(workout.id);
                }
            } else {
                workouts = new Workouts();
                for (WorkoutData workout : workoutData) {
                    workouts.add(new Workout(workout));
                }
                prefs.putString(PREF_WORKOUTS, json.toJson(workouts));
                prefs.flush();
            }
        }
        assets.sounds().enable(prefs.getBoolean(PREF_SOUND, true));
        debug = prefs.getBoolean(PREF_DEBUG, false);
        Gdx.graphics.setContinuousRendering(false);
        ses = Executors.newScheduledThreadPool(2);
        setScreen(new LoadingScreen(this));
//        showWorkouts();
//        for (Graphics.DisplayMode displayMode : Gdx.graphics.getDisplayModes()) {
//            if (displayMode.width == 1920 && displayMode.height == 1200 && displayMode.refreshRate == 60) {
//                Gdx.graphics.setFullscreenMode(displayMode);
//                break;
//            }
//        }
    }

    public Preferences prefs () {
        return prefs;
    }

    public SpriteBatch batch () {
        return batch;
    }

    public ShapeRenderer shapes () {
        return shapes;
    }

    public Workouts workouts () {
        return workouts;
    }

    public Sounds sounds () {
        return assets.sounds();
    }

    public boolean debug() {
        return debug;
    }

    public void debug(boolean debug) {
        this.debug = debug;
    }

    @Override public void resume () {
        super.resume();
    }

    @Override public void pause () {
        persist();
        super.pause();
    }

    public ScheduledExecutorService executor() {
        return ses;
    }

    @Override
    public void dispose () {
        super.dispose();
        assets.dispose();
        shapes.dispose();
        batch.dispose();

        ses.shutdownNow();
        try {
            ses.awaitTermination(3, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void persistWorkouts() {
        prefs.putString(PREF_WORKOUTS, json.toJson(workouts));
        prefs.flush();
    }

    private void persist () {
        prefs.putString(PREF_WORKOUTS, json.toJson(workouts));
        prefs.putBoolean(PREF_SOUND, assets.sounds().enabled());
        prefs.putBoolean(PREF_DEBUG, debug);
        prefs.flush();
    }

    private WorkoutsScreen workoutsScreen;
    public void showWorkouts () {
        if (workoutsScreen == null) {
            workoutsScreen = new WorkoutsScreen(this);
        }
        setScreen(workoutsScreen);
    }
}
