package io.piotrjastrzebski.c25k.utils;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;

public class Sounds {
    boolean enabled = true;
    Sound warmup;
    Sound run;
    Sound walk;
    Sound cooldown;
    Sound complete;

    public Sounds () {

    }

    public void load (AssetManager am) {
        am.load("sounds/warmup.wav", Sound.class);
        am.load("sounds/run.wav", Sound.class);
        am.load("sounds/walk.wav", Sound.class);
        am.load("sounds/cooldown.wav", Sound.class);
        am.load("sounds/workout_complete.wav", Sound.class);
    }

    public void finishLoading (AssetManager am) {
        warmup = am.get("sounds/warmup.wav", Sound.class);
        run = am.get("sounds/run.wav", Sound.class);
        walk = am.get("sounds/walk.wav", Sound.class);
        cooldown = am.get("sounds/cooldown.wav", Sound.class);
        complete = am.get("sounds/workout_complete.wav", Sound.class);
    }

    public void warmup () {
        if (enabled) warmup.play();
    }

    public void run () {
        if (enabled) run.play();
    }

    public void walk () {
        if (enabled) walk.play();
    }

    public void cooldown () {
        if (enabled) cooldown.play();
    }

    public void complete () {
        if (enabled) complete.play();
    }

    public void enable (boolean enabled) {
        this.enabled = enabled;
    }

    public boolean enabled () {
        return enabled;
    }
}
