package io.piotrjastrzebski.c25k.utils;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.PixmapPacker;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader;
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.kotcrab.vis.ui.widget.VisWindow;

public class Assets {
    private AssetManager am;
    private Sounds sounds;

    private final static String FONT = "fonts/Roboto-Bold.ttf";
    private final static String FONT_SMALL = "fonts/roboto24.fnt";
    private final static String FONT_DEFAULT = "fonts/roboto32.fnt";
    private final static String FONT_DEFAULT_OUTLINE = "fonts/roboto32outline.fnt";
    private final static String FONT_LARGE_OUTLINE = "fonts/roboto48outline.fnt";
    private PixmapPacker packer;
    public Assets () {
        am = new AssetManager();

        FreeTypeFontGenerator.setMaxTextureSize(2048);
        am.setLoader(FreeTypeFontGenerator.class, new FreeTypeFontGeneratorLoader(am.getFileHandleResolver()));
        am.setLoader(BitmapFont.class, new FreetypeFontLoader(am.getFileHandleResolver()));

        sounds = new Sounds();
        if (Gdx.app.getType() != Application.ApplicationType.Desktop) {
            VisUI.load(VisUI.SkinScale.X2);
        } else {
            VisUI.load(VisUI.SkinScale.X1);
        }


        final int size = 2048;
        packer = new PixmapPacker(size, size, Pixmap.Format.RGBA8888, 4, false);
//        packer.setTransparentColor(Color.CYAN);
        // start with larger fonts
        load(FONT_LARGE_OUTLINE, FONT, 56, true, packer);
        load(FONT_DEFAULT_OUTLINE, FONT, 32, true, packer);
        load(FONT_DEFAULT, FONT, 32, false, packer);
        load(FONT_SMALL, FONT, 24, false, packer);

        sounds.load(am);

    }

    private void load (String name, String path, int size, boolean outline, PixmapPacker packer) {
        final FreetypeFontLoader.FreeTypeFontLoaderParameter parameter = new FreetypeFontLoader.FreeTypeFontLoaderParameter();
        parameter.fontFileName = path;
        parameter.fontParameters = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.fontParameters.size = size;
        parameter.fontParameters.characters = FreeTypeFontGenerator.DEFAULT_CHARS;
        parameter.fontParameters.characters += "żźłóćąęńśŻŹŁÓĆĄĘŃŚ";
        // Nearest might be better
        parameter.fontParameters.minFilter = Texture.TextureFilter.Linear;
        parameter.fontParameters.magFilter = Texture.TextureFilter.Linear;
        if (outline) {
            parameter.fontParameters.borderColor = new Color(0, 0, 0, .7f);
//            parameter.fontParameters.borderColor = Color.MAGENTA;
            parameter.fontParameters.borderWidth = size/8f;
        }
        parameter.fontParameters.kerning = true;
        parameter.fontParameters.packer = packer;

        am.load(name, BitmapFont.class, parameter);
    }

    private boolean loaded;
    public boolean update () {
        boolean update = am.update();
        if (update && !loaded) {
            loaded = true;
            finishLoading();
        }
        return update;
    }

    public float progress () {
        return am.getProgress();
    }

    private void finishLoading () {
        BitmapFont fontSmall = am.get(FONT_SMALL, BitmapFont.class);
        BitmapFont fontDefault = am.get(FONT_DEFAULT, BitmapFont.class);
        BitmapFont fontDefaultOutline = am.get(FONT_DEFAULT_OUTLINE, BitmapFont.class);
        BitmapFont fontLargeOutline = am.get(FONT_LARGE_OUTLINE, BitmapFont.class);

        if (false) {
            Texture texture = fontSmall.getRegion().getTexture();
            Pixmap pixmap = texture.getTextureData().consumePixmap();
            PixmapIO.writePNG(Gdx.files.external(".c25k/fonts.png"), pixmap);
        }

        Skin skin = VisUI.getSkin();
        skin.getFont("default-font").setUseIntegerPositions(true);
        skin.getFont("small-font").setUseIntegerPositions(true);
        {
            VisLabel.LabelStyle style = skin.get(VisLabel.LabelStyle.class);
            style.font = fontDefault;
        }
        {
            VisLabel.LabelStyle style = skin.get("small", VisLabel.LabelStyle.class);
            style.font = fontSmall;
        }
        {
            VisLabel.LabelStyle style = new Label.LabelStyle(skin.get(VisLabel.LabelStyle.class));
            style.font = fontDefaultOutline;
            skin.add("outline", style);
        }
        {
            VisLabel.LabelStyle style = new Label.LabelStyle(skin.get(VisLabel.LabelStyle.class));
            style.font = fontLargeOutline;
            skin.add("outline-large", style);
        }
        {
            VisTextButton.VisTextButtonStyle style = skin.get(VisTextButton.VisTextButtonStyle.class);
            style.font = fontDefault;
        }
        {
            VisTextButton.VisTextButtonStyle style = skin.get("toggle", VisTextButton.VisTextButtonStyle.class);
            style.font = fontDefault;
        }
        {
            VisWindow.WindowStyle style = skin.get(VisWindow.WindowStyle.class);
            style.titleFont = fontDefault;
        }
        sounds.finishLoading(am);
        packer.dispose();
    }

    private BitmapFont bitmapFont (String path) {
        BitmapFont font = new BitmapFont(Gdx.files.internal(path));
        font.setUseIntegerPositions(true);
        return font;
    }

    public Sounds sounds () {
        return sounds;
    }

    public void dispose () {
        VisUI.dispose();
    }
}
