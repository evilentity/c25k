package io.piotrjastrzebski.c25k.utils;

import java.util.Locale;

public class TextUtils {
    public static String formatDuration (int seconds) {
        int minutes = seconds / 60;
        seconds = seconds % 60;
        return String.format(Locale.ENGLISH, "%d:%02d", minutes, seconds);
    }

    /**
     * Assumes valid format
     * @return duration in seconds
     */
    public static int parseDuration (String duration) {
        String[] split = duration.split(":");
        return Integer.parseInt(split[0]) * 60 + Integer.parseInt(split[1]);
    }
}
