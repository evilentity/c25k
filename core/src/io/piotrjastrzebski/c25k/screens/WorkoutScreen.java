package io.piotrjastrzebski.c25k.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.kotcrab.vis.ui.util.dialog.Dialogs;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;
import io.piotrjastrzebski.c25k.C25KApp;
import io.piotrjastrzebski.c25k.data.Workout;
import io.piotrjastrzebski.c25k.data.WorkoutStep;
import io.piotrjastrzebski.c25k.data.WorkoutType;
import io.piotrjastrzebski.c25k.utils.Log;
import io.piotrjastrzebski.c25k.utils.TextUtils;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Screen for current workout, in progress
 */
public class WorkoutScreen extends BaseScreen {
    private final Workout workout;
    private WorkoutStep currentStep;
    private boolean started = false;
    private boolean paused = false;
    private int nextStepId = 0;
    private int totalSeconds;
    private int totalStepSeconds;
    private int secondsElapsed;
    private int secondsStepElapsed;
    private float timer;
    private VisLabel remainingTime;
    private VisLabel stepType;
    private VisLabel remainingStepTime;
    private VisTable remainingStepTable;
    private float timeScale = 1;

    private float startButtonRadius = 96;
    private Actor startActor;
    private long startTouchTime = 0;
    private float startTouchDuration = .5f;
    private float startAnimTime = 0;
    private float startAnimDuration = 0.2f;

    public WorkoutScreen (C25KApp app, Workout workout) {
        super(app);
        this.workout = workout;
        if (app.debug()) {
            timeScale = 60;
        }
        {
            VisTextButton cancel = new VisTextButton("Cancel");
            root.add(cancel).pad(8).row();
            cancel.addListener(new ChangeListener() {
                @Override public void changed (ChangeEvent event, Actor actor) {
                    cancelWorkout();
                }
            });
        }
        {
            startActor = new Actor();
            root.add(startActor).pad(64, 8, 64, 8).size(startButtonRadius * 2, startButtonRadius * 2).row();
            startActor.addListener(new ActorGestureListener(20, 0.4f, startTouchDuration, 0.15f) {
                @Override public void tap (InputEvent event, float x, float y, int count, int button) {
                    if (!started) {
                        startWorkout();
                    } else {
                        if (paused) {
                            resumeWorkout();
                        }
                    }
                }

                @Override public void touchDown (InputEvent event, float x, float y, int pointer, int button) {
                    super.touchDown(event, x, y, pointer, button);
                    if (pointer == 0) {
                        startTouchTime = TimeUtils.nanoTime();
                    }
                }

                @Override public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
                    if (pointer == 0) {
                        startTouchTime = 0;
                    }
                }

                @Override public boolean longPress (Actor actor, float x, float y) {
                    if (!paused) {
                        startTouchTime = 0;
                        pauseWorkout();
                    }
                    return true;
                }
            });
        }
        remainingTime = new VisLabel("", "outline-large");
        remainingTime.setAlignment(Align.center);
        root.add(remainingTime).pad(32, 8, 32, 8).height(128 + 64).fill().row();
        {
            remainingStepTable = new VisTable();
            stepType = new VisLabel("", "outline");
            remainingStepTable.add(stepType).pad(4).row();
            remainingStepTime = new VisLabel("", "outline-large");
            remainingStepTable.add(remainingStepTime);
            root.add(remainingStepTable).pad(32, 8, 32, 8).height(128 + 64).expandX().fill().row();
        }
        root.add().expand().fill().row();
        totalSeconds = workout.data.duration();
        currentStep = workout.data.steps.get(workout.stepId);
        for (int i = 0; i < workout.stepId - 1; i++) {
            secondsElapsed += workout.data.steps.get(i).duration();
        }
        secondsElapsed += workout.stepProgressSeconds;
        secondsStepElapsed = workout.stepProgressSeconds;
        totalStepSeconds = currentStep.duration();
    }

    private void pauseWorkout () {
//        startAnimTime = startAnimDuration;
        paused = true;
        cancelRendering();
    }

    private void resumeWorkout () {
        startAnimTime = startAnimDuration;
        paused = false;
        playSound();
        scheduleRendering();
    }

    private void playSound () {
        if (currentStep == null) return;
        switch (currentStep.type) {
        case RUN: app.sounds().run();
            break;
        case WALK: app.sounds().walk();
            break;
        case WARMUP: app.sounds().warmup();
            break;
        case COOLDOWN: app.sounds().cooldown();
            break;
        }
    }

    @Override public void render (float delta) {
        if (started && !paused) {
            timer += delta * timeScale;
            WorkoutStep prevStep = currentStep;
            while (timer >= 1) {
                timer -= 1;
                secondsElapsed += 1;
                secondsStepElapsed += 1;
                workout.stepProgressSeconds = secondsStepElapsed;
//                Log.info("Tick");
                if (secondsStepElapsed >= totalStepSeconds) {
                    nextStep();
                }
            }
            remainingTime.setText(TextUtils.formatDuration(totalSeconds - secondsElapsed));
            remainingStepTime.setText(TextUtils.formatDuration(totalStepSeconds - secondsStepElapsed));
            if (prevStep != currentStep) {
                playSound();
            }
        }
        super.render(delta);
        // TODO show minute/30s notches
        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        ShapeRenderer shapes = app.shapes();
        shapes.setProjectionMatrix(app.batch().getProjectionMatrix());
        shapes.begin(ShapeRenderer.ShapeType.Filled);
        {
            float cx = startActor.getX(Align.center);
            float cy = startActor.getY(Align.center);
            shapes.setColor(.4f, .4f, .4f, 1);
            shapes.circle(cx, cy, startButtonRadius, 64);
            shapes.setColor(.6f, .6f, .6f, 1);
            shapes.circle(cx, cy, startButtonRadius - 3, 64);

            shapes.setColor(.4f, .4f, .4f, 1);
            if (!started || paused) {
                // |>
                float s = startButtonRadius;
                shapes.triangle(
                    cx - s * .35f, cy + s * .5f,
                    cx + s * .55f, cy,
                    cx - s * .35f, cy - s * .5f
                );
            } else {
                float w = startButtonRadius * .2f;
                float h = startButtonRadius;
                // ||
                shapes.rect(cx - w * 1.5f, cy - h/2, w, h);
                shapes.rect(cx + w * 0.5f, cy - h/2, w, h);
            }

            shapes.setColor(1f, 1f, 1f, .6f);
            if (startAnimTime > 0) {
                float a = MathUtils.clamp(1 - startAnimTime/startAnimDuration, 0, 1);
                shapes.circle(cx, cy, (startButtonRadius - 3) * a, 64);
                startAnimTime -= delta;
                Gdx.graphics.requestRendering();
            } else if (startTouchTime != 0 && !paused && started) {
                float progress = (float)((TimeUtils.nanoTime() - startTouchTime)/(double)1000000000L)/startTouchDuration;
                float a = MathUtils.clamp(progress, 0, 1);
                shapes.circle(cx, cy, (startButtonRadius - 3) * a, 64);
                Gdx.graphics.requestRendering();
            }
        }
        {
            float x = remainingTime.getX();
            float y = remainingTime.getY();
            float width = remainingTime.getWidth();
            float height = remainingTime.getHeight();
            shapes.setColor(.4f, .4f, .4f, 1);
            shapes.rect(x, y, width, height);
            {
                float ix = x + 2;
                float iy = y + 2;
                float iw = width - 4;
                float ih = height - 4;
                Array<WorkoutStep> steps = workout.data.steps;
                int warmUpSeconds = steps.get(0).duration();
                int cooldownSeconds = steps.get(steps.size-1).duration();
                int midSeconds = totalSeconds - warmUpSeconds - cooldownSeconds;

                shapes.setColor(colorWalk);
                float h = ih * .2f - 2;
                shapes.rect(ix, iy, iw, h);
                shapes.rect(ix, iy + ih * .8f + 2, iw, h);

                shapes.setColor(colorNotch);
                for (int i = 60; i <= warmUpSeconds - 60; i+=60) {
                    float ox = i/(float)warmUpSeconds;
                    shapes.rect(ix + ox * iw, iy + ih * .8f + 2, 2, h);
                }
                for (int i = 60; i <= cooldownSeconds - 60; i+=60) {
                    float ox = i/(float)cooldownSeconds;
                    shapes.rect(ix + ox * iw, iy, 2, h);
                }


                for (int i = 1; i < steps.size - 1; i++) {
                    WorkoutStep step = steps.get(i);
                    if (step.type == WorkoutType.RUN) {
                        shapes.setColor(colorRun);
                    } else {
                        shapes.setColor(colorWalk);
                    }
                    float sa = (step.from - warmUpSeconds)/(float)midSeconds;
                    float ea = (step.to - warmUpSeconds)/(float)midSeconds;
                    float sx = iw * sa;
                    float sw = iw * ea - sx;
                    shapes.rect(ix + sx, iy + ih * .2f, sw, ih * .6f);
                }
                shapes.setColor(colorNotch);
                for (int i = 60; i <= midSeconds - 60; i+=60) {
                    float ox = i/(float)midSeconds;
                    shapes.rect(ix + ox * iw, iy + ih * .2f, 2, ih * .6f);
                }
            }
        }
        {
            float x = remainingStepTable.getX();
            float y = remainingStepTable.getY();
            float width = remainingStepTable.getWidth();
            float height = remainingStepTable.getHeight();
            shapes.setColor(.4f, .4f, .4f, 1);
            shapes.rect(x, y, width, height);
            if (currentStep.type == WorkoutType.RUN) {
                shapes.setColor(colorRun);
            } else {
                shapes.setColor(colorWalk);
            }
            shapes.rect(x + 2, y + 2, width - 4, height - 4);
        }
        {
            float x = remainingTime.getX();
            float y = remainingTime.getY();
            float width = remainingTime.getWidth();
            float height = remainingTime.getHeight();

            Array<WorkoutStep> steps = workout.data.steps;
            int warmUpSeconds = steps.get(0).duration();
            int cooldownSeconds = steps.get(steps.size-1).duration();
            int midSeconds = totalSeconds - warmUpSeconds - cooldownSeconds;
            shapes.setColor(colorOver);
            if (nextStepId -1 == 0) { // warm up
                float a = MathUtils.clamp((secondsElapsed)/(float)warmUpSeconds, 0, 1);
                shapes.rect(x, y + height * .8f, width * a, height * .2f);
                shapes.setColor(colorEdge);
                shapes.rect(x + width * a - 1, y + height * .8f, 2, height * .2f);
            } else if (nextStepId == workout.data.steps.size) { // cooldown
                float a = MathUtils.clamp((secondsElapsed - warmUpSeconds - midSeconds)/(float)cooldownSeconds, 0, 1);
                shapes.rect(x, y, width * a, height * .2f);
                // fill mid and warm up
                shapes.rect(x, y + height * .2f, width, height * .8f);
                shapes.setColor(colorEdge);
                shapes.rect(x + width * a - 1, y, 2, height * .2f);
            } else { // rest
                float a = MathUtils.clamp((secondsElapsed - warmUpSeconds)/(float)midSeconds, 0, 1);
                shapes.rect(x, y + height * .2f, width * a, height * .6f);
                // fill warm up
                shapes.rect(x, y + height * .8f, width, height * .2f);
                shapes.setColor(colorEdge);
                shapes.rect(x + width * a -1, y + height * .2f, 2, height * .6f);
            }
        }
        {
            float a = secondsStepElapsed/(float)totalStepSeconds;
            float x = remainingStepTable.getX();
            float y = remainingStepTable.getY();
            float width = remainingStepTable.getWidth();
            float height = remainingStepTable.getHeight();
            shapes.setColor(colorOver);
            shapes.rect(x, y, width * a, height);
            float ew = Math.min(12, width * a);
            shapes.rect(x + width * a - ew, y, ew, height, Color.CLEAR, colorEdge, colorEdge, Color.CLEAR);

            shapes.setColor(colorNotch);
            int step = totalStepSeconds <= 5 * 60?30:60;
            for (int i = 0; i <= totalStepSeconds; i+=step) {
                float ox = i/(float)totalStepSeconds;
                if (totalStepSeconds <= 5 * 60) {
                    if (i % 60 == 0) {
                        shapes.rect(x + ox * width, y, 2, height);
                    } else {
                        shapes.rect(x + ox * width, y + height * .25f, 2, height * .5f);
                    }
                } else {
                    if (i % (5 * 60) == 0) {
                        shapes.rect(x + ox * width, y, 2, height);
                    } else {
                        shapes.rect(x + ox * width, y + height * .25f, 2, height * .5f);
                    }
                }
            }
        }
        shapes.end();
        stage.act(delta);
        stage.draw();
    }

    private Color colorNotch = new Color(.4f, .4f, .4f, 1f);
    private Color colorEdge = new Color(.2f, .2f, .2f, .5f);
    private Color colorWalk = new Color(.6f, .6f, .6f, 1);
    private Color colorRun = new Color(.75f, .75f, .75f, 1);
    private Color colorOver = new Color(.3f, .3f, .3f, .5f);

    private void rect (float x, float y, float width, float height, Color color) {
        ShapeRenderer shapes = app.shapes();
        shapes.setColor(.4f, .4f, .4f, 1);
        shapes.rect(x, y, width, height);
        shapes.setColor(color);
        shapes.rect(x + 2, y + 2, width - 4, height - 4);
    }

    ScheduledFuture<?> rendering;
    private void startWorkout () {
        Log.info("Start workout");
        started = true;
        paused = false;
        totalSeconds = workout.data.duration();
        startAnimTime = startAnimDuration;
        currentStep = workout.data.steps.get(workout.stepId);
        secondsStepElapsed = workout.stepProgressSeconds;
        nextStepId = workout.stepId + 1;
        totalStepSeconds = currentStep.duration();
        stepType.setText(currentStep.type.name());
        playSound();
        scheduleRendering();
    }


    private void nextStep () {
        if (nextStepId >= workout.data.steps.size) {
            finishWorkout();
            return;
        }
        workout.stepId = nextStepId;
        workout.stepProgressSeconds = 0;
        currentStep = workout.data.steps.get(nextStepId++);
        secondsStepElapsed = 0;
        totalStepSeconds = currentStep.duration();
        stepType.setText(currentStep.type.name());
        Log.info("Next step " + currentStep);
    }

    private void finishWorkout () {
        Log.info("Workout done!");
        cancelRendering();
        started = false;
        app.sounds().complete();
        remainingTime.setText("");
        remainingStepTime.setText("");
        root.clearChildren();
        workout.completed = true;
        workout.stepId = 0;
        workout.stepProgressSeconds = 0;
        app.persistWorkouts();
        Dialogs.showConfirmDialog(stage, "All done!", "Congrats!", new String[] {"AWESOME!"},
            new Boolean[] {Boolean.TRUE}, result -> {
                app.showWorkouts();
            });
    }

    @Override public void resume () {
        super.resume();
        Gdx.graphics.requestRendering();
        // a bit annoying, there are levels of pause on ios...
        // if we open either top or bottom drawer/menu we get pause notification
        // but we are still working
        // if we press home or turn off the device we then actually stop things
        if (!paused && started) {
            scheduleRendering();
        }
    }

    @Override public void pause () {
        super.pause();
        app.persistWorkouts();
        cancelRendering();
    }

    private void cancelWorkout () {
        paused = true;
        cancelRendering();
        app.persistWorkouts();
        if (started) {
            Dialogs.ConfirmDialog<Boolean> dialog = Dialogs
                .showConfirmDialog(stage, "Cancel?", "Are you sure?", new String[] {"YES", "NO"},
                    new Boolean[] {Boolean.TRUE, Boolean.FALSE}, result -> {
                        if (result) {
                            app.showWorkouts();
                        }
                    });
            dialog.setMovable(false);
            dialog.setModal(true);
        } else {
            app.showWorkouts();
        }
    }

    private void scheduleRendering () {
        cancelRendering();
        // how often?
        long delay = 1000;
        if (app.debug()) {
            delay /= 60;
        }
        rendering = app.executor().scheduleAtFixedRate(() -> {
            Gdx.graphics.requestRendering();
        }, 0, delay, TimeUnit.MILLISECONDS);
    }

    private void cancelRendering () {
        if (rendering != null) rendering.cancel(true);
    }
}
