package io.piotrjastrzebski.c25k.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.MathUtils;
import io.piotrjastrzebski.c25k.C25KApp;

public class LoadingScreen extends BaseScreen {
    public LoadingScreen (C25KApp app) {
        super(app);
    }

    float c = 0;
    @Override public void render (float delta) {
        Gdx.gl.glClearColor(c, c, c, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        if (app.assets.update()) {
            app.showWorkouts();
        }
        c = MathUtils.lerp(0, .5f, app.assets.progress());
        // so we keep updating
        Gdx.graphics.requestRendering();
        fpsLogger.log();
    }
}
