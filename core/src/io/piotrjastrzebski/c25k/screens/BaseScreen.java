package io.piotrjastrzebski.c25k.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import io.piotrjastrzebski.c25k.C25KApp;

public class BaseScreen implements Screen {
    public final static int WIDTH = 720;
    public final static int HEIGHT = 1280;
    protected final C25KApp app;
    protected final Viewport viewport;
    protected final OrthographicCamera camera;
    protected final Stage stage;
    protected final Table root;
    protected FPSLogger fpsLogger;

    public BaseScreen (C25KApp app) {
        this.app = app;
        camera = new OrthographicCamera();
        viewport = new ScreenViewport(camera);
        stage = new Stage(viewport, app.batch());
        root = new Table();
        root.setFillParent(true);
        root.setName("screen-root");
        stage.addActor(root);

        fpsLogger = new FPSLogger();
    }

    @Override public void show () {
        Gdx.input.setInputProcessor(stage);
    }

    @Override public void render (float delta) {
        Gdx.gl.glClearColor(.5f, .5f, .5f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        fpsLogger.log();
    }

    @Override public void resize (int width, int height) {
        viewport.update(width, height, true);
    }

    @Override public void pause () {

    }

    @Override public void resume () {

    }

    @Override public void hide () {

    }

    @Override public void dispose () {

    }
}
