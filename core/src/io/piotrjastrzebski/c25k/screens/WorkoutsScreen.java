package io.piotrjastrzebski.c25k.screens;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.kotcrab.vis.ui.util.dialog.Dialogs;
import com.kotcrab.vis.ui.widget.*;
import io.piotrjastrzebski.c25k.C25KApp;
import io.piotrjastrzebski.c25k.data.Workout;
import io.piotrjastrzebski.c25k.data.Workouts;
import io.piotrjastrzebski.c25k.utils.Log;

/**
 * List of all workouts
 */
public class WorkoutsScreen extends BaseScreen {
    Workouts workouts;
    public WorkoutsScreen (C25KApp app) {
        super(app);
        workouts = app.workouts();
    }

    @Override public void show () {
        super.show();
        root.clear();
        root.add(new VisLabel("WORKOUTS")).pad(8).row();

        VisTable buttons = new VisTable();
        root.add(buttons).growX().row();
        VisTextButton debugSpeedToggle = new VisTextButton("DEBUG", "toggle");
        buttons.add(debugSpeedToggle).size(200, 90).expandX().pad(8);
        debugSpeedToggle.setChecked(app.debug());
        debugSpeedToggle.addListener(new ChangeListener() {
            @Override public void changed (ChangeEvent event, Actor actor) {
                app.debug(debugSpeedToggle.isChecked());
            }
        });

        buttons.add().size(200, 90).growX();

        VisTextButton soundToggle = new VisTextButton("SOUND", "toggle");
        soundToggle.setChecked(app.sounds().enabled());
        soundToggle.addListener(new ChangeListener() {
            @Override public void changed (ChangeEvent event, Actor actor) {
                app.sounds().enable(soundToggle.isChecked());
            }
        });
        buttons.add(soundToggle).size(200, 90).expandX().pad(8);

        VisTable wc = new VisTable(true);
        VisScrollPane scrollPane = new VisScrollPane(wc);
        scrollPane.setScrollingDisabled(true, false);
        root.add(scrollPane).expand().fill().pad(8).row();
        for (int i = 0, week = 1; i < workouts.size; i+=3, week += 1) {
            wc.add(new VisLabel("Week " + week, "small")).colspan(3).expandX().center().row();
            wc.add(workoutView(workouts.get(i))).expand();
            wc.add(workoutView(workouts.get(i + 1))).expand();
            wc.add(workoutView(workouts.get(i + 2))).expand();
            wc.row();
            wc.add().pad(8);
            wc.row();
        }
    }

    private Actor workoutView (final Workout workout) {
        VisTable content = new VisTable(true);
        content.add(new VisLabel("#" + (workout.id + 1))).expandX().left();
        VisLabel label = new VisLabel("Done!", "small");
        VisTextButton button = new VisTextButton("Start!");

        if (workout.completed) {
            label.setText("Done!");
            button.setText("Again!");
        } else if (workout.stepId != 0) {
            label.setText("In progress");
            button.setText("Resume?");
        } else {
            label.setText("Not done!");
            button.setText("Start");
        }
        int percentDone = workout.progress();
        label.setText(percentDone + "%");
        content.add(label).row();
        content.add(button).size(200, 90).colspan(2);
        button.addListener(new ChangeListener() {
            @Override public void changed (ChangeEvent event, Actor actor) {
                startWorkout(workout);
            }
        });

        return content;
    }

    @Override public void render (float delta) {
        super.render(delta);
        stage.act(delta);
        stage.draw();
    }

    private void startWorkout (Workout workout) {
        if (workout.completed) {
            Dialogs.showConfirmDialog(stage, "Again?", "Do the workout again?", new String[] {"YES", "CANCEL"},
                new Boolean[] {Boolean.TRUE, Boolean.FALSE}, result -> {
                    if (result) {
                        workout.completed = false;
                        workout.stepId = 0;
                        workout.stepProgressSeconds = 0;
                        showWorkout(workout);
                    }
                });
        } else if (workout.stepId != 0) {
            final int resume = 0;
            final int restart = 1;
            final int cancel = 2;
            Dialogs.showConfirmDialog(stage, "Workout in progress", "Resume or restart?", new String[] {"RESUME", "RESTART", "CANCEL"},
                new Integer[] {resume, restart, cancel}, result -> {
                    switch (result) {
                    case resume: {
                        showWorkout(workout);
                    } break;
                    case restart: {
                        workout.stepId = 0;
                        workout.stepProgressSeconds = 0;
                        showWorkout(workout);
                    } break;
                    case cancel: {

                    } break;
                    }
                });
        } else {
            showWorkout(workout);
        }
    }

    private void showWorkout (Workout workout) {
        app.setScreen(new WorkoutScreen(app, workout));
    }
}
