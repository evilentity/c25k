package io.piotrjastrzebski.c25k.desktop;

import com.badlogic.gdx.tools.hiero.Hiero;

import javax.swing.*;

public class HieroLauncher {
    public static void main (final String[] args) {
        // text
        // do we want suits? they are not in cabin font
        // ♠♥♦♣♤♡♢♧
        // □ for any missing symbols
        /* Glyphs used so far
ABCDEFGHIJKLMNOPQRSTUVWXYZŻŹĆÓŁĘĄŚŃ
abcdefghijklmnopqrstuvwxyzżźćółęąśń
1234567890
"!`?'.,;:()[]{}<>|/\^$-%+=#_&~*□
         */
        SwingUtilities.invokeLater(() -> new Hiero(args));
    }
}
