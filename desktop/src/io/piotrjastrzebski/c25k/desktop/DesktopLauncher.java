package io.piotrjastrzebski.c25k.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import io.piotrjastrzebski.c25k.C25KApp;

public class DesktopLauncher {
    public static void main (String[] arg) {
        Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
        config.setWindowedMode(720/2, 1280/2);
        config.setPreferencesConfig(".c25k/", Files.FileType.External);
        config.setWindowSizeLimits(1, 1, 9999, 9999);
        new Lwjgl3Application(new C25KApp(), config);
    }
}
